/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oaklife1;

import java.io.IOException;
import java.util.Random;

/**
 *
 * @author erik.f
 * 
 */

public class Profile {
    
    static void introduction(){ //Introduces the game - nothing difficult
        System.out.println("Welcome to Oak life!");
        promptEnterKey();
         System.out.println("This game is a life simulator, where you will make choices which influence events to come!");
        promptEnterKey();
        System.out.println("The following is your birth certificate: ");
        System.out.println("");
        
    }
    static void createProfile(){
        
        Random rn = new Random(); //random function renamed to rn
        String[] maleNames  = {"Alexis", "Ben", "Christian", "David", "Ethan", "Faris", "Gabriel", "Helmut", "Ian", "Johnny"}; //10 random male names
        String[] femaleNames = {"Kara", "Lea", "Mia", "Natalie", "Olivia", "Perla", "Quinn", "Rachel", "Sara", "Tamara"}; //10 random female names
        String[] lastNames = {"Ufford", "Fuchs", "Khalifa", "Texas", "Yemen", "Zayne", "Apple", "Baxner", "Macht", "Dover"}; // 10 random last names
        int random1 =  rn.nextInt(10); //chooses the male name index
        int random2 =  rn.nextInt(10); //chooses the female name index
        int random3 =  rn.nextInt(10); //chooses last name index
        
        int gender = rn.nextInt(2); //choose whether character is male or female
        
        if (gender==0){//if male
            String name = maleNames[random1] + " " + lastNames[random3]; //Concatonate the strings (make the name)
            System.out.println("Name (first and last): " + name); //print name
            
            System.out.println("Gender: Male"); //print gender
        }
        else{ //if female
            String name = femaleNames[random2] + " " +  lastNames[random3]; //Concatonate the strings (make the name)
            System.out.println("Name (first and last): " + name); //print name
            System.out.println("Gender: Female"); //print gender
        }
        
        
        String[] months = {"Januray", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}; //array of months
        String[] days = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}; //array of days of the month
        String[] year = {"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"}; // array of possible years of birth
        String birthday = months[rn.nextInt(12)] + " " + days[rn.nextInt(30)] + ". " + year[rn.nextInt(26)]; //create the birthday by concatonating the strings
        System.out.println("Date of Birth:  " + birthday);  //print the birthday
        String[] countries = {"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Former Yugoslavian Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Palestine?", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"}; //List of all countries
        String nationality = countries[rn.nextInt(239)];
        System.out.println("Nationality: " + nationality); //print random nationality from the 239 options
        String characters[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String iN = "";
        for (int i =0; i<11; i++){
            iN = iN + characters[rn.nextInt(36)];
        }
        System.out.println("Identity number: " + iN );
    }

    static void promptEnterKey(){
    System.out.println("Press \"ENTER\" to continue...");
    try {
        System.in.read();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
    
}
